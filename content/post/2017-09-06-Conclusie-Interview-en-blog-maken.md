---
title: Conclusie Interview en blog maken
date: 2017-09-06
---

# Conclusie Interview en blog maken

Vandaag heb ik mijn interview uitgeschreven, net als de andere uit mijn team. Hierbij heb ik naar mijn opgenomen bestand geluisterd met de aantekeningen naast mij. Tijdens het beluisteren van mijn interview was ik in het begin tevreden, maar al snel veranderde dat omdat ik op sommige momenten niet had doorgevraagd terwijl dat wel van toepassing had kunnen zijn. 

> Goed leermoment hierbij is: interview wat beter voorbereiden/organiseren en eventueel alvast de doorvragen toevoegen om mijn interview lijst.

Nadat dit is gedaan heb ik mijn bestand op de drive gezet zodat Rick de antwoorden van de interviews bij elkaar kan zetten voor een beter overzicht.

Toen dat gedaan was hebben we individueel gekeken naar de dingen die ons opvielen. Daaruit werd duidelijk dat de interesse over de architectuur bij de vierdejaars sterker is dan bij een tweedejaars. Ook de kennis over de architectuur in Rotterdam is veelzijdig. Sommige kende ze wel en andere hadden ze geen idee (Dit gelde voor alle studenten).

Na het individueel analyseren hebben we met elkaar besproken over het geen wat we te weten zijn gekomen. Wat ons opviel en wat het plan voor de volgende paar dagen zal zijn. We hebben afgesproken om vrijdag onze concept ideeën op de google drive te zetten zodat we allemaal elkaars ideeën/concepten kunnen zien. Maandag gaan we dan ook kijken met welk concept(en) we verder gaan, en of we deze eventueel deze gaan combineren.

**Blog**
Om 13:00 kregen we onze eerste workshop, namelijk het maken van je eigen blog. In plaats van Wordpress met HTML codes werken we nu met Markdown. Via de site stackedit maken we hierbij onze blogs en reflectie. Voor mijn mening vind ik deze manier wel makkelijker dan wordpress, waar ik eerder mee gewerkt heb. Toch heb ik nog vele vragen erover. Zoals de verschillende Markdown bestanden combineren tot een site. En om de afbeeldingen toe te voegen. Maar we hebben te horen gekregen dat we dit volgende week te horen krijgen. 

**Studiecoach**
Na de workshop over het maken van je eigen blog hadden we een moment met onze Studiecoach. Hierbij hebben we de opdracht gekregen om een SWOT-Analyse te maken van ons team. Dit was wel even lastig omdat het team verband met ons best wel goed werkt. Het was dan ook best lastig om de vuilkuilen en de zwakke punten van ons team te vinden. Toch kwamen we eruit dat we toch op sommige momenten te makkelijk of te moeilijk denken. 

Ook kregen we de opdracht om een teamposter te maken met onze afspraken in ons team. Hier gaan wij morgen na de Design Theorie mee verder.
