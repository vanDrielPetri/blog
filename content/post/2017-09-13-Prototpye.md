---
title: Prototype
date: 2017-09-13
---

# Prototype

Vandaag zijn we bezig geweest met het maken van ons prototype. We hadden gisteren (dinsdag) overlegd wie wat zou maken voor de schermen van ons prototype. Ik moest de schermen maken voor het ontvangen van een materiaal kaart bij een gebouw en een pop-up van de materialen die je in bezit hebt.
Deze hebben we naast elkaar gelegd en hebben ze ook daarna op volgorde gezet. Nadat we dat gedaan hadden hebben we een paar proefpersonen uit de klas gehaald om te kijken of ze het spel wel snapte.
De conclusie daaruit kwam dat het erg onduidelijk was. Dus hebben we een grote plattegrond erbij gemaakt in de hoop dat ze nu wel het speelveld zouden snappen.
We zijn toen weer op zoek gegaan naar andere personen om de game te beoordelen. Deze vonden het begin duidelijk, maar later kwam er toch wel iets van verwarring. 
Na dat gesprek hebben we Jantien (onze docent) erbij gehaald met de vraag of het spel duidelijk was. Zij vertelde ons dat we het spel beter anders kunnen neerleggen,
nu hadden we namelijk de schermen naast elkaar gelegd en met pijlen aangewezen waar je naar toe komt als je er op klikt. Zo zouden we een stapel kunnen maken van de schermen en als de persoon erop klikt, je het scherm van de stapel
afhaald om dan het nieuwe scherm te tonen.

**Moodboard en onderzoek collage**
Na die feedback heb ik ook mijn moodboard van de doelgroep aan haar laten zien, deze vond zij goed en ik hoefde er niks meer aan te veranderen. Bij mijn collage daarin tegen was nog best wel wat om aan te geven. Zelf was ik niet zo tevreden erover
en voor haar was het ook best onduidelijk. Ik moest er een verhaal bij vertellen zodat ze het beter begreep. Dus moest ik er even op terugkijken en nagaan wat ik beter kan doen om het via beeld duidelijk te maken voor anderen.
Ze steld ook voor om een infographic te maken. Dit vond ik een goed idee en ben daarmee verder gegaan met kijken.

**Studiecoaching**
We hadden studiecoaching les, in de les moesten we twee test maken over de rol die ik zou hebben in een groep. En mijn manier van leren. Met de uitslagen van deze test moesten we een uitleg geven of we er wel of niet mee eens waren. Uit de eerste test was ik best verrast toen ik het woord "bedrijfsman" zag staan. 
Toen ik het woord zag was ik er gelijk niet mee eens. Maar toen ik begon te lezen had ik wel een gevoel van hé dit zou eigenlijk best wel kloppen. Bij de manier van leren kwam uit nadenkende leerstijl. Daar was ik het een beetje mee eens.
Vooral omdat er stond beschreven dat ik vele uitleg te makkelijk vind geformuleerd en dat vind ik echt totaal niet. 
We kregen toen te horen dat we nog drie andere opdrachtn moesten maken en deze inleveren vrijdag voor 17:00. Bij een van die opdrachten moesten we 6 doelen bepalen die we wilde halen per kwartaal.
Voor mij was dat heel erg moeilijk en ik had werkelijk geen idee. Dus schreef ik maar op dat ik een animatie filmpje wilde maken. Maar dat was er maar 1. Dus heb ik hulp gevraagd aan een van de studiecoaches omdat ik echt niks wist.
Ze begon mijn enige doel te lezen er ze vertelde me dat er al vier doelen te lezen waren. Het eerste doel heb ik toen ontleed op zoek naar de verschillende doelen die daarin verwerkt waren. Nu hoefde ik er nog maar 2. 

Ik heb toen ook besloten om donderdag middag aan de rest van de opdrachten te werken zodat ik deze donderdag avond al kon inleveren op N@tschool


