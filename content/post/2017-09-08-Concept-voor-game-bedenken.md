---
title: Concept voor game bedenken
date: 2017-09-08
---

# Concept voor game bedenken

_Vandaag ben ik bezig geweest met het bedenken van een game dat geschikt zal zijn voor de doelgroep_

Allereerst ben ik naar mijn conclusie wezen kijken van de interviews die zijn gedaan. Daaruit kwam naar voren dat de studenten die aan de opleiding zijn begonnen grotendeels van een MBO opleiding komen, en dus meer wilde leren over het vak. Sommige zijn aan deze opleiding begonnen doordat hun ouders of een ander familielid al in het vak zitten. 
Verder zijn de studenten best positief over de stad Rotterdam. Ook al kennen sommige de stad niet zo goed, ze blijven positief. Wat ze vooral interessant vinden aan Rotterdam is de ontwikkeling van gebouwen en het contrast van oude en nieuwe architectuur.
Het meeste waar iedereen aan denkt in Rotterdam is een grote stad met havens en hoge gebouwen.
Verder kwam ik ook op de ontdekking dat de 4e jaars studenten meer interesse in architectuur hebben dan bij 2e jaars studenten.
Ook is het social media gebruik niet heel erg sterk, wel wordt er het meeste gemaakt van Facebook en Instagram.

**Gamen**
Op het game vlak is een groot deel van de studenten niet erg geïnteresseerd. Het gaat hierom de online games zoals console games, app en pc games. Wel vinden ze het belangrijk dat als je een spel (game) doet dat de gezelligheid en vermaak op nummer 1 moet staan. Dus de interactie is vooral het allerbelangrijkste in de game. 

> Als ik terug kijk naar het interview, vind ik dat ik me veel beter had moeten voorbereiden en ook nog wat andere vragen erbij moet stellen bijvoorbeeld: Wat doe je het liefste in je vrije tijd? En wat is de game die je als gespeeld hebt (alleen of samen)? Zoals in mijn vorige blog al beschreven staat had ik ook meer moeten doorvragen.

Na de conclusie ben ik gaan kijken welke richting het beste op kan gaan. Ik heb toen lijsten gemaakt met soorten games, zo had ik apps, games console, pc games, borderellen en kaartspellen. Bij deze onderwerpen ben gaan zoeken naar games die daar onder passen en heb deze onder het onderwerp geschreven. 
Zo kwam ik uit op:

**Kaartspellen**
 - Pesten/Uno
 - Poker
 - Klaverjassen
 - Canasta
 - Kwartet

**Bordspellen**

 - Mens erger je niet
 - Monopoly
 - Kolonisten van Catan
 - Rummikub
 - Stratego

**Apps**
 - Pokémon Go
 - Candy Crush
 - Garden Scapes
 - Flappy Birds
 - Subway Surf

*Verdere lijsten en namen van spelletjes zijn te vinden in mijn dummy*

Ik ben ook online gegaan om te kijken of er nieuwe spellen zijn waar ik niet zo veel van af weet. Daar kwam ik games/spelletjes tegen waar ik nog nooit van heb gehoord zoals: Machi Koro, Skip.Bo, Machiavelli enz.

Het waren spellen die echt iets nieuws hadden zo kan je met Machi Koro (een kaartspel) een stad maken, door bepaalde producten te verzamelen zodat er een upgrade van kwam. Ook vond ik het idee van een Escape Room een heel goed idee. Omdat je dan in een teamverband wat moet oplossen, en Escape Rooms worden ook steeds populairder.

Dit spel bracht bij mij een idee. Want wat als je in het spel ook al de studenten kennis leert te maken met de architectuur van Rotterdam (aangezien het thema architectuur is). In het begin dacht ik aan een bordspel waarbij je als een soort monopoly plekken moet kopen en als je de drie plekken hebt, een Rotterdams gebouw krijgt. Het doel is dan ook de meeste Rotterdamse gebouwen krijgen. Maar snel dacht ik al, dit gaat best lang duren. Misschien toch maar verder kijken naar iets dat wat korter duurt.
Toch bedacht ik me dat het ook gewoon een kaartspel kan zijn. Waarbij je drie kaarten moet verzamelen en daarna met die kaarten een gebouw kan "bouwen/kopen" De vraag was nu wat voor drie kaarten? Proces kaarten die je ondergaat tijdens de opleiding. Een goed idee, maar toch lastig omdat je heel snel de gebouwen kan kopen. Uiteindelijk ben ik erop gekomen om onderdelen van het gebouw te doen. Zo moet je drie materiaal kaarten verzamelen om het gebouw te kopen.  Dit idee werd het en gelijk ben ik wezen kijken naar welke gebouwen ik ga gebruiken (en de materialen ervan opzoeken). Zo heb ik tien gebouwen gevonden waarbij sommige heel modern zijn en andere wat ouder. Het was even zoeken maar uiteindelijk had ik tien gebouw kaarten en 30 materiaal kaarten. Hierbij kwam het aantal kaarten op 40 en dat vond ik toch wel wat saai, vooral omdat het echt alleen maar om verzamelen gaat en niet om de interactie. Dus heb ik 12 andere kaarten erbij gedaan dit zijn cadeau kaarten en pest kaarten. Zo heb je een beurt overslaan kaart, lever een gebouw in, gebouw cadeau, steel een kaart van een ander enz. Ook is er wel een online element erbij. Zo kan je een app downloaden die bij het kaartspel hoort. In de app is een simpele versie van de plattegrond van Rotterdam te zien. Als je het gebouw hebt kan je een code invoeren in de app (deze code staat op de achterkant van de vier kaarten). Als je dat gedaan hebt verschijnt het gebouw dat je hebt gewonnen in 3d op de plattegrond, je kan het gebouw zelfs claimen door je naam bovenin te zetten. Maar deze kan ook verdwijnen als je het gebouw kwijtraakt.
*De hele speluitleg kan je hieronder lezen.*

**Concept 2**
Toch wilde ik nog een concept creëer, maar deze is meer bedoelt voor de app. Dit idee had ik al snel in mijn hoofd en dat kwam vooral omdat de inspiratie van Draw it en Pokémon go kwam. In de app krijg je 10 opdrachten die je moet beantwoorden of maken. Zo krijg je tekenopdrachten, vragen over gebouwen enz. Deze opdrachten staan in het teken van architectuur maar ook krijg je al proefjes van de opleiding. Zo kan je al een bouwtekening natekenen, of je eigen architectuur bedenken. Als je de 10 opdrachten hebt hebt gedaan dan krijg je de opdracht om een gebouw in Rotterdam te zoeken en een foto ervan te nemen. Als je dat doet wordt krijg je weer 10 gratis opdrachten. Dit blijft zich dan herhalen.

Ook is er een interactie want je kan ervoor kiezen om samen te spelen of alleen. Als je voor interactie kiest, kan je samen de opdrachten maken, of tegen elkaar strijden. Je kan ook teams maken en strijden tegen andere teams. 
Er is ook een score lijst waar je kan zien hoe hoog je staat met je team, of alleen.

>Het proces van het creëer van het concept ging voor mij best goed. Ik heb een concept gemaakt waarbij ik goed heb gekeken naar de doelgroep en de ander weer wat minder. Ik heb tijdens mijn onderzoek meer naar offline spellen gekeken, dit omdat de doelgroep niet veel interesse toont in online. Misschien had ik ook naar online games moeten kijken waar daar kan je ook dingen uithalen die ook geschikt kunnen zijn voor offline games.

Nadat ik mijn twee concepten af had heb ik deze geplaatst op de gezamenlijke drive van onze groep gezet. Dit hebben we namelijk afgesproken, zo kunnen we in het weekend elkaars concepten inlezen en maandag een beslissing maken met welke concepten we verder gaan.