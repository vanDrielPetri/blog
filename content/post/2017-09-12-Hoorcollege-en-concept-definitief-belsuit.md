---
title: Concept definitief besluit
date: 2017-09-12
---

# Concept definitief besluit

Vandaag zijn we verder gegaan met het concept er moest namelijk nog een interactief element in voor komen. In de morgen zat ik in de trein en ben ik bezig geweest met het bedenken voor een idee. Het was even zoeken en denken, maar uiteindelijk is er een idee ontstaan. 

**Namelijk:**
Iedere speler zit in een team en moet samen met het team door heel Rotterdam materialen verzamelen om zo een gebouw in Rotterdam te maken. Dit doen ze door als een soort Pokémon Go, door heel Rotterdam te lopen en naar architecture gebouwen/construcies langs te lopen. Als je bij zo een gebouw/constructie aankomt kan je materialen ophalen, die je weer een stapje verder brengen naar het maken van het gebouw. Als je de materialen hebt moet je naar het gebouw lopen (in real life) om daar het gebouw te "bouwen". Als dat lukt krijg je op de kaart (in je telefoon) je gebouw in 3d te zien. Dit blijf zo doorgaan tot dat je alle gebouwen hebt.

Na het hoorcollege dat we vandaag hadden. Heb ik het idee aan het team voorgesteld. Het idee vonden ze goed en daaruit zijn we verder gegaan met het het bedenken hoe we het als prototype willen laten afbeelden/vormgeven. We hebben samen een lijst gemaakt met beelden die we in het prototypen voor willen laten komen. Zo kwamen we op 10 dingen uit. Deze zijn te lezen in de afbeelding hieronder.

Nadat we de lijst hadden gemaakt, hadden we een taakverdeling gemaakt met wie wat zal maken. Zo zal ik de *Kaart* die je op de *locatie* vind *ontvangen* En een *kaart* met *pop-up* van *materialen*. Hiermee ben ik ook verder gegaan heb heb deze afgerond.

> Wat ik vandaag geleerd heb is vooral gebeurd tijdens de hoorcollege over Verbeelden. Ik had namelijk nooit gehoord van Gestalt Wetten, en terwijl er voorbeelden naar voren kwamen begreep ik het goed, het waren ook dingen die ik al eerder had gezien. Maar van die termen had ik echt nog nooit van gehoord. 