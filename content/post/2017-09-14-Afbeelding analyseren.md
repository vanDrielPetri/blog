---
title: Afbeeldingen analyseren
date: 2017-09-14
---

# Afbeeldingen analyseren

Vandaag had ik mijn tweede Design Theorie les. Voor deze les moest ik vijf verschillende soorten afbeeldingen meenemen. 

In het begin van de les kregen we papier uitgedeeld met verschillende vakjes. In die vakjes moesten we binnen 10 seconde een woord verbeelden met simpelen vormen. Ik vond het een goede en ook hele interessante oefening. Op sommige momenten was het best wel moeilijk vooral de emoties zoals verlegen en brutaal, deze kan ik makkelijk uittekenen maar met een paar simpele lijnen vind ik zelf nog best lastig.

Nadat we dat gedaan hadden moesten we onze afbeeldingen uitknippen en lijmen op een a4 papier. Als we dat gedaan hadden moesten we de afbeeldingen analyseren en kijken welke gestalt wetten erin voor kwamen in de foto. Voor sommige afbeeldingen vond ik het best makkelijk. Zo had ik een afbeelding van een wenteltrap in de vorm van een klok. Daar kwam de wet van continuïteit in voor en voor en achtergrond. Maar ik had ook een afbeelding van een hinde die midden in de foto stond. Dat was wat lastiger. Vooral omdat ik gisteren (woensdag) een workshop had in beeldanalyse en de term symmetrisch net weer iets anders is dan bij de gestalt wetten. Toch vond ik wel dat het bij die foto paste omdat de hinde midden in de foto stond. 

> Ik vond het een hele interessante les. Vooral omdat altijd wel een van die wetten wel eens voorkomt in een afbeelding. Ook vind ik het interessant dat er goed wordt nagedacht over de compositie zodat het beeld een stuk interessanter en aangenamer word als je er naar kijkt.

Na deze les ben ik naar huis gegaan en ben ik thuis hard bezig geweest met het maken van nog een paar opdrachten die vrijdag ingeleverd moeten. Voor het visueel onderzoek had ik even moeite. Maar met een kleine inspiratie van Pinterest heb ik een lijn gevonden voor het maken van mijn onderzoek. Deze heb ik thuis op een a3 blad getekend. Daar heb ik een foto van gemaakt en in het inlever document gezet. Ook moest ik nog de 6 doelen uitschrijven die ik per kwartaal wil halen. Het was even zoeken maar uiteindelijk had ik de laatste twee gevonden. Meer leren over het programma sketch en minder perfectionistisch zijn. Toen ik deze opdrachten af haf heb ik alles in één document gezet en deze opgeslagen als pdf. Nadat ik dat had gedaan. Had ik het ingeleverd op N@tschool.

Voor de teamopdrachten moesten we één iemand in ons team aanwijzen die het voor ons zou inleveren. We hebben toen gekozen voor Rick. Hij heeft het teamdocument ook donderdag avond ingeleverd.