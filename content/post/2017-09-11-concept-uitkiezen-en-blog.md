---
title: Concept uitkiezen en blog maken
date: 2017-09-11
---

# Concept uitkiezen en blog maken

Vandaag zijn we bezig geweest met het uitkiezen van een concept. Afgelopen vrijdag heeft iedereen individueel een concept op de gezamenlijke drive gezet. We vertelde onze concepten aan elkaar en hebben er uiteindelijk één uitgekozen. Het concept waar we mee verder wilde gaan was het kaartspel dat ik had bedacht. We hadden alleen wel een probleem, er was niet echt een online aspect of iets waarbij de studenten Rotterdam beter leren kennen. 

> Er is daar ook feedback op gegeven om meer te richten om het speelveld nog groter te maken en ook om de studenten (met de game) wat van Rotterdam te laten zien/leren kennen.
> Ook was er feedback om niet te uitgebreid te gaan met het creëer van een concept. Een goede tip die ik zeker mee zal nemen.

We zijn bezig geweest met het bedenken om het online aspect erbij toe te voegen. Het is een lastige klus en we zijn hier nog steeds mee bezig. Het is namelijk best lastig en voor een goede terugblik is het beter om de volgende keer alle ideeën op te schrijven, op internet kijken naar mogelijkheden (voor inspiratie). En als laatste ook kijken/contacten met de doelgroep om te zien of het idee hun wel wat lijkt.

**Blog**
Ook zijn we bezig geweest met de workshop voor het maken van een blog. Dit was best lastig, vooral omdat de volgorde van het geen dat je moet doen door elkaar heen staat. 
Het was een lastig proces, maar uiteindelijk heb ik het voor elkaar gekregen om de blogs post eindelijk te plaatsen op de webstite. Wat nog wel moet gebeuren is de datum op een één of andere manier te veranderen (hij staat nu namelijk op 1 january 0001). Dus dat wordt nog even zoeken, ook de titels worden niet op de juiste wijze weergeven (terwijl deze de wel is aangegeven met de juiste code)

> Als ik erop terug kijk met het maken van het blog moet ik toegeven dat ik het maken van de teksten in stackedit ideaal vind. Het is makkelijk en duidelijk en de codes voor schuine en vette letters top. 
> MAAARRR..... het maken en creëer van de website om de blogs erop te posten was heel ingewikkeld. Ik moet er nog meer in verdiepen om meer te leren over de website. Gelukkig weet ik nu wel hoe ik het bericht moet plaatsten dat is alweer een hele geruststelling.