---
title: Onderzoek en  de doelgroep
date: 2017-09-04
---

# onderzoek en de doelgroep

Vandaag zijn we in teamverband bezig geweest met het bepalen voor wie we de game gaan maken (onze doelgroep). We kwamen er snel uit dat Rotterdam veel architectuur heeft en dat een goed idee zal zijn voor onze game. Maar toen kwam de vraag welke studenten van de Hogeschool Rotterdam hebben de meeste interesse in de architectuur. Op de site van de Hogeschool hebben we gekeken naar alle studies en er een uitgekozen die volgens ons de meeste interesse zouden hebben in de architectuur. Hieruit kwamen we op de studenten die de opleiding Bouwkunde volgen.

**Onderzoek doelgroep**
Na het besluiten van de doelgroep is er een research geweest naar de opleiding. Zo heb ik onderzocht wat de precies allemaal te leren krijgen op de opleiding en hoe hun proces zou gaan lopen. Zo kwamen er een paar punten naar boven zoals:

 - Technische tekeningen maken en lezen
 - Kennis van constructies
 - Kennis van bouwsystemen en materialen
 - Kennis van bouwfysica
 - Organiseren
 - Samenwerken en leidinggeven
 - Oog voor actualiteit
 - Welke rol als bouwkundig expert kan je spelen op de werkvloer

Ook zijn we wezen kijken hoeveel procent van de studenten slaagt, overgaat en de opleiding dolgraag over wilt doen. Verder is er ook gekeken naar de hoeveel studenten die de opleiding volgen en hoeveel jongens en meisjes daarvan zijn. 

![](doelgroep-keuze.jpg)
![](doelgroep-keuze-2.jpg)

**Interview**
We hebben een vragenlijst gemaakt voor de studenten zodat we meer over de doelgroep te weten zouden komen. We wilde eerst een enquete maken en deze opsturen naar de leerlingen bouwkunde over de mail. Maar we kregen als tip dit beter niet te doen omdat je dan niet verder op je vragen in kan gaan. Ook kregen we als tip om verder te kijken naar andere manieren om de persoon inteviewen zoals het gebruik maken van een plattegrond. Uiteindelijk is er gekozen om een paar gebouwen uit Rotterdam uit te printen en deze mee te nemen tijdens het interview met de vraag of ze de gebouwen herkennen en of ze misschien de bijnamen weten.

**Moodboard**
Vanuit het onderzoek ben ik bezig geweest met het creëer van een moodboards voor de doelgroep. Uit het onderzoek dat ik hierboven heb verteld ben ik bezig geweest met het maken van de moodboard. Voor de moodboards heb ik in iedere geval twee studenten die hard aan het werk zijn met het bedenken van bouwwerken. Mijn eerste moodboards zag er ook heel zwaar uit, met studenten zwaar in de boeken en hard aan het werk. En ook al heeft de opleiding theorie het geeft niet de juiste sfeer vind ik. Bij mijn tweede optie had ik dezelfde elementen maar dan met het nachtleven van amsterdam en vrolijke studenten. Dit gaf alleen nog steeds niet de sfeer die ik zoek, dit omdat er op de moodboard alleen maar foto's van gebouwen en bouwtekeningen te zien waren. Uiteindelijk bij mijn derde moodboards heb ik het hele proces van de bouw laten een op de achtergrond. Eerst de tekentafel, proefbouw maken, 3d visual op de computer, de bouwval en uiteindelijk de constructie it self.