---
title: Design Theorie in praktijk
date: 2017-09-07
---
 
# Design Theorie in praktijk
Vandaag had ik de eerste Design theorie les. Hierbij had ik mijn aantekeningen van mijn hoorcollege nodig. 
In de les moesten we een proces visueel weergave op papier. Het was voor even nadenken, maar uiteindelijk had ik het proces gekozen voor de verandering van mijn kamer. Tijdens het uittekenen van mijn proces kwam ik er toch wel achter dat ik toch op sommige momenten lastig vind om het visueel uit te beelden. Vooral omdat ik het heel fijn vind om een paar tekeningen te maken met daarbij veel uitleg ernaast. Gelukkig ging het met dit proces wel goed. 

Nadat dat gedaan was, moesten we in teamverband ons proces laten zien en een keuze maken met welk proces we verder wilde gaan om dat daarna te presenteren. We zijn voor het proces van Marleen gedaan die haar proces liet zien van het maken van een kostuum. Hierbij zijn we gezamenlijk wezen overleggen met welke processen er nog meer aan te pas zouden voort komen. Dit was niet zo moeilijk en uiteindelijk hadden we best snel alles op papier. Sverre en Marleen zijn toen gaan presenteren.

Iris en Ik hebben toen nog de opdracht gekregen om een presentatie te beoordelen van een andere groep. Het was alleen lastig omdat we hem niet zo goed konden verstaan. We hadden eigenlijk moeten ingrijpen en moeten vragen of hij wat duidelijker wilde praten, maar dat hadden we niet gedaan. Toch van het geen dat ik heb verstaan klonk het proces goed en waren er niet echt dingen waar je op kan merken.
