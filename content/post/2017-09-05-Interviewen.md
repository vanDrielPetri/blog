---
title: Interviewen
date: 2017-09-05
---

# Interviewen

*Vandaag zijn we met het hele team na de hoorcollege naar de Hogeschool Rotterdam gegaan op de locatie Academieplein.* 

Dit om de studenten van de bouwkunde te kunnen interviewen. Om te beginnen splitte we op in twee groepen zodat ze makkelijker de studenten 1 op 1 konden interviewen. Het was even zoeken vooral omdat we in het begin eerst naar de verkeerde plek werden gewezen. Na een zoektocht waren we eindelijk op een verdieping waar studenten van bouwkunde te vinden waren. Iris en ik vonden op die verdieping vier tweedejaars studenten van bouwkunde en die hebben we geïnterviewd. We namen het gesprek op via mijn telefoon en hadden de papieren met vragen en de foto's van de gebouwen. 

**Aantekeningen**
Iris ging de studenten interviewen terwijl ik aantekeningen ervan ging maken, om te kijken of er al wat dingen opvielens tijdens het gesprek. Het interview ging heel goed en het was een fijn gesprek. In het begin probeerde ik ook het hele gesprek over te schrijven, dit ging alleen lastig omdat sommige vragen heel snel werden beantwoord.  Toen heb ik besloten om de belangrijkste aantekeningen op te schrijven. Wat mij wel opviel was dat het de interesse in games er laag was bij de vier studenten. 

**Mijn interview**
Na het interview kwamen we de andere van het team weer tegen. Samen gingen toen de afdeling af op zoek naar bouwkunde studenten. Het was wel lastig want ze waren moeilijk te vinden. En als we er een paar gevonden hadden zaten ze in de les. En zelfs als de les afgelopen was hadden ze geen tijd omdat ze gelijk door naar de volgende les moesten. 
Na nog een keer te hebben gevraagd waar we bouwkunde studenten kunnen vinden en weer naar een andere verdieping werden gestuurd kwamen we terecht bij het centrale punt van de studie bouwkunde. Daar waren genoeg studenten aan het werk. Ik keek om me heen om te kijken of er sommige studenten wel gestoord konden worden en niet heel hard aan het werk waren. Uiteindelijk heb ik een vierdejaars student gevonden die tijd had. Tijdens het interview kwam ik erachter dat de interesse in architectuur een stuk hoger lag dan de tweedejaars studenten. 
En ook hij had (net als een van de vorige studenten voorheen) als motivatie voor de studie dat zijn vader ook al in het vak zat en het hem heel leuk en interessant leek. 

> Toch als ik nu terugkijk naar het interview had ik bij sommige vragen meer door moeten vragen en vooral met het woord waarom. 